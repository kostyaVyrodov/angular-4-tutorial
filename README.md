# Angular CLI commands

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Generate service

`ng generate service <service name>`

## Generate component

`ng generate component <component name>`

